package mis.citas.hackaton.servicio.impl;

import mis.citas.hackaton.modelo.Empleado;
import mis.citas.hackaton.servicio.RepositorioEmpleado;
import mis.citas.hackaton.servicio.ServicioEmpleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServicioEmpleadoImpl implements ServicioEmpleado {

    @Autowired
    RepositorioEmpleado repositorioEmpleado;

    @Override
    public Empleado findByUser(String user) {
        Optional<Empleado> r = this.repositorioEmpleado.findById(user);
        return r.isPresent()?r.get() : null;
    }

    @Override
    public void addEmpleado(Empleado e) {
        this.repositorioEmpleado.insert(e);
    }

    @Override
    public ResponseEntity saveEmpleadoByUser(String user, Empleado e) {
        this.repositorioEmpleado.save(e);
        e.setIdEmpleado(user);
        return new ResponseEntity<>("Empleado actualizado correctamente.", HttpStatus.OK);
    }
}

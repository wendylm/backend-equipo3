package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Cita;

import java.util.Date;
import java.util.List;

public interface ServicioCita {

     public void agregar(Cita c);
     public List<Cita> obtenerTodos();
     public List<Cita> buscarPorDescripcion(String nombreDescripcion);
     public void ajustarEstatusCitaPorId(String idCita,String estatus);


}

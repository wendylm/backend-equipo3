package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Cita;
import mis.citas.hackaton.modelo.Empleado;

import java.util.List;

public interface ServicioAcceso {
    public Empleado comprobarAcceso(String user);

}

package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Cita;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RepositorioCita extends MongoRepository<Cita, String> {

    @Query("{'idPerfil' : ?0 , $and : [ { 'estatus' :'Abierto' }  ] }")
    public List<Cita> buscarPorDescripcion(String nombreDescripcion);

}

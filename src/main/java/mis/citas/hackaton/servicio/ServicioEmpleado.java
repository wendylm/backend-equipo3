package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Empleado;
import org.springframework.http.ResponseEntity;

public interface ServicioEmpleado {

    public Empleado findByUser(String user);

    public void addEmpleado(Empleado e);

    public ResponseEntity saveEmpleadoByUser(String user, Empleado e);
}

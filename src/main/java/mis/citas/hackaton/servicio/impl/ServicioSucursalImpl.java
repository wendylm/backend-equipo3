package mis.citas.hackaton.servicio.impl;

import mis.citas.hackaton.modelo.Sucursal;
import mis.citas.hackaton.servicio.RepositorioSucursal;
import mis.citas.hackaton.servicio.ServicioSucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class ServicioSucursalImpl implements ServicioSucursal {

    @Autowired
    RepositorioSucursal repositorioSucursal;

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void agregar(Sucursal c) {
        c.setId(null);
        this.repositorioSucursal.insert(c);
    }

    @Override
    public Sucursal findByNombre(String nombre) {
        Optional<Sucursal> r = this.repositorioSucursal.findById(nombre);
        return r.isPresent()?r.get() : null;
    }
}
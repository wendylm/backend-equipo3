package mis.citas.hackaton.modelo;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Sucursal {

    
    String id;
    String idSucursal;
    @Id String nombre;
    String ubicacion;

    public Sucursal(String id, String idSucursal, String nombre, String ubicacion) {
        this.id = id;
        this.idSucursal = idSucursal;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}


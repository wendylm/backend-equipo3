package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Sucursal;

public interface ServicioSucursal {

    public void agregar(Sucursal c);
    public Sucursal findByNombre(String nombre);
}

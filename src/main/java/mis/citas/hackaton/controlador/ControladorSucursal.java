package mis.citas.hackaton.controlador;

import mis.citas.hackaton.modelo.Sucursal;
import mis.citas.hackaton.servicio.ServicioSucursal;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.type.LogicalType.DateTime;

@RestController
@RequestMapping("/sucursal")
public class ControladorSucursal {

    @Autowired
    ServicioSucursal servicioSucursal;

    @GetMapping("/{nombre}")
    public Sucursal getSucursalByNombre(@PathVariable String nombre) {
        return this.servicioSucursal.findByNombre(nombre);
    }

    @PostMapping
    public void guardarSucursal(@RequestBody Sucursal c) {
        c.setId(null);
        this.servicioSucursal.agregar(c);
    }

}
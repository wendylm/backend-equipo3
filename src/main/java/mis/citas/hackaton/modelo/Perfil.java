package mis.citas.hackaton.modelo;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Perfil {

    
    String id;
    @Id String idPerfil;
    String descripcion;

    public Perfil(String id, String idPerfil, String descripcion
    ) {
        this.id = id;
        this.idPerfil = idPerfil;
        this.descripcion = descripcion;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}


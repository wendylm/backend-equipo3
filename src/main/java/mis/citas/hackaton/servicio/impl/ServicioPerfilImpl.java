package mis.citas.hackaton.servicio.impl;

import mis.citas.hackaton.modelo.Perfil;
import mis.citas.hackaton.servicio.RepositorioPerfil;
import mis.citas.hackaton.servicio.ServicioPerfil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class ServicioPerfilImpl implements ServicioPerfil {

    @Autowired
    RepositorioPerfil repositorioPerfil;

    @Override
    public Perfil findByNombre(String perfil) {
        Optional<Perfil> r = this.repositorioPerfil.findById(perfil);
        return r.isPresent()?r.get() : null;
    }
}
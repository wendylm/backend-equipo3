package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Perfil;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface RepositorioPerfil extends MongoRepository<Perfil, String> {

    @Query("{'perfil' : {$elemMatch : {'perfil':?0}}}")
    public List<Perfil> findByNombre(String perfil);
}
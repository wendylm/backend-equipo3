package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Empleado;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface RepositorioEmpleado extends MongoRepository<Empleado, String> {

    @Query("{'empleado' : {$elemMatch : {'user':?0}}}")
    public List<Empleado> findByUser(String user);
}

package mis.citas.hackaton.modelo;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Empleado {

    String idEmpleado;
    @Id String user;
    String idSucursal;
    String idPerfil;
    String nombre;
    String cita;

    public Empleado(String idEmpleado,String user, String idSucursal, String idPerfil, String nombre,String cita) {
        this.idEmpleado = idEmpleado;
        this.user = user;
        this.idSucursal = idSucursal;
        this.idPerfil = idPerfil;
        this.nombre = nombre;
        this.cita = cita;

    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCita() {
        return cita;
    }

    public void setCita(String cita) {
        this.cita = cita;
    }
}

package mis.citas.hackaton.controlador;


import mis.citas.hackaton.modelo.Cita;
import mis.citas.hackaton.servicio.ServicioCita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.type.LogicalType.DateTime;

@RestController
@RequestMapping("/cita/v1/")
@CrossOrigin
public class ControladorCita {

    @Autowired
    ServicioCita servicioCita;

    @GetMapping("/{descripcion}/")
    public List<Cita> obtenerCitas(@RequestParam(required = true) String fecha, @PathVariable String descripcion ) throws ParseException {
        System.out.println("idPerfil"+descripcion);
        List<Cita> citasDev = new ArrayList<>();

        if(descripcion.equals("Director")){
            return this.servicioCita.obtenerTodos();
        }
        else if(descripcion!=null || fecha!=null){
           List<Cita> citas = this.servicioCita.buscarPorDescripcion(descripcion);
           for(Cita cita:citas){
               SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
               Date d1 = sdformat.parse(fecha);
               Date d2 = sdformat.parse(sdformat.format(cita.getHorario()));
               if(d1.compareTo(d2) == 0) {
                   citasDev.add(cita);
               }
           }
            return citasDev;
        }

        return null;
    }

    @PostMapping
    public void guardarCita(@RequestBody Cita c) {
        c.setId(null);
        this.servicioCita.agregar(c);
    }


    public static class EstatusUsuario {
        public String estatus;
    }


    @PatchMapping("/{id}")
    public void ajustarEstatus(
                            @PathVariable String id,
                            @RequestBody EstatusUsuario estatus) {

         this.servicioCita.ajustarEstatusCitaPorId(id, estatus.estatus);
    }

}

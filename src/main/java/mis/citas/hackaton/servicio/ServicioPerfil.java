package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Perfil;

public interface ServicioPerfil {

	public Perfil findByNombre(String nombre);
}

package mis.citas.hackaton.controlador;

import mis.citas.hackaton.modelo.Empleado;
import mis.citas.hackaton.seguridad.JwtBuilder;
import mis.citas.hackaton.servicio.ServicioAcceso;
import mis.citas.hackaton.servicio.ServicioEmpleado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/acceso/v1/")
@CrossOrigin(origins= "*")
public class ControladorLogin {

    @Autowired
    ServicioAcceso servicioAcceso;

    @Autowired
    JwtBuilder jwtBuilder;

    @Autowired
    ServicioEmpleado servicioEmpleado;


    public static class Login {
        public String usuario;
        public String contrasenia;
    }

    public static class AccesoCorrecto {

        public Empleado usuario;
        public String Mensaje;
        public String token;

        public AccesoCorrecto(Empleado usuario, String mensaje, String token) {
            this.usuario = usuario;
            Mensaje = mensaje;
            this.token = token;
        }

    }

    @PostMapping
    public AccesoCorrecto acceso(@RequestBody Login c) {
         Empleado empleado = this.servicioEmpleado.findByUser(c.usuario);
       // System.out.println(empleado.getUser());
        if(empleado!=null && c.contrasenia.equals("ClaveMaestra")){
            String token = jwtBuilder.generarToken(c.usuario);
            

            return new AccesoCorrecto(empleado,"AccesoCorrecto",token);
        }else{
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Sin acceso");
        }
    }
}

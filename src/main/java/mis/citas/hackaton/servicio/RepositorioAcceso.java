package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Cita;
import mis.citas.hackaton.modelo.Empleado;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioAcceso  extends MongoRepository<Empleado, String> {

    @Query("{'idEmpleado' : ?0 }")
    public Empleado comprobarAcceso(String user);
}

package mis.citas.hackaton.servicio.impl;

import com.mongodb.client.result.UpdateResult;
import mis.citas.hackaton.modelo.Cita;
import mis.citas.hackaton.servicio.RepositorioCita;
import mis.citas.hackaton.servicio.ServicioCita;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ServicioCitaImpl implements ServicioCita {

    @Autowired
    RepositorioCita repositorioCita;

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void agregar(Cita c) {
        c.setId(null);
        this.repositorioCita.insert(c);
    }

    @Override
    public List<Cita> obtenerTodos() {
        return this.repositorioCita.findAll();
    }

    @Override
    public List<Cita> buscarPorDescripcion(String nombreDescripcion) {
        return this.repositorioCita.buscarPorDescripcion(nombreDescripcion);
    }

    @Override
    public void ajustarEstatusCitaPorId(String id, String estatus) {

        final Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(new ObjectId(id)));

        final Update u = new Update();
        u.set("estatus", estatus);
        u.set("actualizacion", new java.util.Date());

        final UpdateResult r = this.mongoOperations.updateFirst(q, u, Cita.class);



    }
}

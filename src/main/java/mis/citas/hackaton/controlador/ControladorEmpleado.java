package mis.citas.hackaton.controlador;

import mis.citas.hackaton.modelo.Empleado;
import mis.citas.hackaton.servicio.ServicioEmpleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/empleados")
@CrossOrigin(origins= "*")
public class ControladorEmpleado {

    @Autowired
    ServicioEmpleado servicioEmpleado;

    @GetMapping("/{user}")
    public Empleado getEmpleadoByUser(@PathVariable String user) {
        return this.servicioEmpleado.findByUser(user);
    }

    @PostMapping
    public void createEmpleado(@RequestBody Empleado empleado){
        //empleado.setIdEmpleado(null);
        this.servicioEmpleado.addEmpleado(empleado);
    }

    @PutMapping("/{user}")
    public ResponseEntity putEmpleadoByUser(@PathVariable String user, @RequestBody Empleado empleado){
        final Empleado p = this.servicioEmpleado.findByUser(user);
        if (p == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else{
            return this.servicioEmpleado.saveEmpleadoByUser(user, empleado);
        }
    }

}

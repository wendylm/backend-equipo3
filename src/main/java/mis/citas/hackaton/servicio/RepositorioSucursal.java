package mis.citas.hackaton.servicio;

import mis.citas.hackaton.modelo.Sucursal;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface RepositorioSucursal extends MongoRepository<Sucursal, String> {

    @Query("{'sucursal' : {$elemMatch : {'nombre':?0}}}")
    public List<Sucursal> findByNombre(String nombre);
}
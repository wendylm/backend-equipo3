package mis.citas.hackaton.servicio.impl;

import mis.citas.hackaton.modelo.Empleado;
import mis.citas.hackaton.servicio.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServicioAccesoImpl implements ServicioAcceso {


    @Autowired
    RepositorioAcceso repositorioAcceso;

    @Override
    public Empleado comprobarAcceso(String user) {
        return this.repositorioAcceso.comprobarAcceso(user);
    }

}
